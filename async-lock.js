/*
  Asynchronous lock that can be awaited in async functions.
  If the lock is enabled, `await` will wait for the lock to be disabled before
  proceeding. If the lock is disabled, `await` will proceed as normal.

  This pattern can be very useful in scenarios like these:
  1. You have some functions that make requests to an API that requires frequent
     reauthentications or robust fault tolerance
  2. Your code is structured in micro-modules that depend on each other's async
     initialization
  3. You want to resolve a promise from outside of the promise's scope
*/

class AsyncLock {
  constructor () {
    this.disable = () => {}
    this.promise = Promise.resolve()
  }

  enable () {
    this.promise = new Promise(resolve => this.disable = resolve)
  }
}

/*
  EXAMPLE USAGE:
*/

// Create a new lock
const lock = new AsyncLock()

// Enable it
lock.enable()

// Make an async function that...
async function test () {
  // Waits for the lock to be disabled
  await lock.promise

  // Then writes to STDOUT
  console.log('Test')
}

// Call the async function. If the procedure ended here, 'Test' would never be
// written to STDOUT, because the lock would stay enabled forever, and `lock.promise`
// would never resolve
test()

// After 3000ms, call `lock.disable()`. `lock.promise` will resolve, and 'Test'
// will finally be printed to STDOUT. You could reenable the lock afterwards if
// you wish to.
setTimeout(() => lock.disable(), 3000)